package com.labas.sample;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
//import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.CountDownTimer;
import android.os.IBinder;
//import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BroadcastService extends Service {

    private WifiManager wifiManager;

//    private final static String TAG = "BroadcastService";

    public static final String COUNTDOWN_BR = "com.labas.sample.cd";
    Intent bi = new Intent(COUNTDOWN_BR);

    CountDownTimer cdt = null;

    private Integer timeLimit = -1;

    @Override
    public void onCreate() {
        if (timeLimit == -1)
            return;
        super.onCreate();

        wifiManager = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
        Toast.makeText(BroadcastService.this, "Wi-Fi veiks " + timeLimit + " minut" + (timeLimit % 10 == 1 && timeLimit != 11 ? "ę" : (timeLimit % 10 > 9 ? "čių" : "es")), Toast.LENGTH_SHORT).show();

        bi.putExtra("BROWSE_TIME", timeLimit);
        sendBroadcast(bi);

        final Date currentDate = Calendar.getInstance().getTime();
        final String date = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
        final String started = new SimpleDateFormat("HH:mm:ss").format(currentDate);

        timeLimit = ((timeLimit * 60) - 1) * 1000;
//        Log.i(TAG, "Starting timer... ->" + timeLimit);
        cdt = new CountDownTimer(timeLimit, 1000) {
            private long likesLaikas = 0;

            @Override
            public void onTick(long millisUntilFinished) {
                likesLaikas = millisUntilFinished / 1000;
//                Log.i(TAG, "Countdown seconds remaining: " +  likesLaikas);
                bi.putExtra("likesLaikas", likesLaikas);
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                cdt.cancel();
                timeLimit = -1;
                sendBroadcast(bi.putExtra("likesLaikas", (long)timeLimit));
                wifiManager.setWifiEnabled(false);
                Toast.makeText(BroadcastService.this, "Interneto naršymo laikas pasibaigė!", Toast.LENGTH_SHORT).show();

//                SharedPreferences.Editor spreEdit = getSharedPreferences("nData", MODE_WORLD_WRITEABLE).edit();
//                spreEdit.putString("date", date);
//                spreEdit.putString("started", started);
//                spreEdit.putString("finished", (new SimpleDateFormat("HH:mm:ss")).format(Calendar.getInstance().getTime()));
//                spreEdit.putInt("cost", Math.max(1, (int)Math.ceil((float)(bi.getIntExtra("BROWSE_TIME", 0)*60 - likesLaikas)/60)));
//                spreEdit.apply();

                //Saugoti duomenis duomenų bazėje iš karto.
//                SharedPreferences spref = getSharedPreferences("nData", MODE_WORLD_WRITEABLE);
                ReportDBHandler handler = new ReportDBHandler(BroadcastService.this);
                handler.addEntry(new ReportEntry(0, date, started, new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()), Math.max(1, (int)Math.ceil((float)(bi.getIntExtra("BROWSE_TIME", 0)*60 - likesLaikas)/60))));
//                handler.addEntry(new ReportEntry(0, spref.getString("date", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())), spref.getString("started", "00:00:00"), spref.getString("finished", "00:00:01"), spref.getInt("cost", 1)));
//                spreEdit.clear().apply();
            }
        };

        cdt.start();
    }

    @Override
    public void onDestroy() {
        cdt.cancel();

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (timeLimit == -1) {
            timeLimit = intent.getExtras().getInt("seekArc");
            onCreate();
        } else {
            cdt.onFinish();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}