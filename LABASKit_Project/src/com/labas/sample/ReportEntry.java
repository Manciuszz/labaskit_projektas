package com.labas.sample;

public class ReportEntry {

    private int mID, mCost;
    private String mDate, mStarted, mFinished;

    public ReportEntry() {
        mID = 0;
        mDate = "";
        mStarted = "";
        mFinished = "";
        mCost = 0;
    }

    public ReportEntry(int id, String Date, String Started, String Finished, int Cost) {
        mID = id;
        mDate = Date;
        mStarted = Started;
        mFinished = Finished;
        mCost = Cost;
    }

    public void setIndex(int val) { mID = val; }
    public int getIndex()
    {
        return mID;
    }

    public void setDate(String val)
    {
        mDate = val;
    }
    public String getDate() { return mDate; }

    public void setStarted(String val)
    {
        mStarted = val;
    }
    public String getStarted() { return mStarted; }

    public void setFinished(String val)
    {
        mFinished = val;
    }
    public String getFinished() { return mFinished; }

    public void setCost(int val) { mCost = val; }
    public int getCost()
    {
        return mCost;
    }
}
