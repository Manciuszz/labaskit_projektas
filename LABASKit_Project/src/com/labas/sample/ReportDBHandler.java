package com.labas.sample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class ReportDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "reports.db";

    private static String TABLE_NAME = "reports";

    private final static String KEY_ID = "id";
    private final static String KEY_DATE = "date";
    private final static String KEY_STARTED = "started";
    private final static String KEY_FINISHED = "finished";
    private final static String KEY_COST = "cost";

    public ReportDBHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_DATE + " DATE," +
                KEY_STARTED + " TEXT," +
                KEY_FINISHED + " TEXT," +
                KEY_COST + " INTEGER" +
                ")";
        System.out.println(query);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        String query = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(query);
        onCreate(db);
    }

    public long addEntry(ReportEntry entry)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(KEY_DATE, entry.getDate());
        cv.put(KEY_STARTED, entry.getStarted());
        cv.put(KEY_FINISHED, entry.getFinished());
        cv.put(KEY_COST, entry.getCost());

        long id = db.insert(TABLE_NAME, null, cv);
        db.close();
        return id;
    }

    public ReportEntry getEntry(int id)
    {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        cursor = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_DATE, KEY_STARTED, KEY_FINISHED, KEY_COST }, KEY_ID + "=?", new String[] { Integer.toString(id) }, null, null, null);

        ReportEntry entry = new ReportEntry();
        if(cursor != null)
        {
            if(cursor.moveToFirst())
            {
                entry.setIndex(cursor.getInt(0));
                entry.setDate(cursor.getString(1));
                entry.setStarted(cursor.getString(2));
                entry.setFinished(cursor.getString(3));
                entry.setCost(cursor.getInt(4));
            }
        }

        cursor.close();
        db.close();

        return entry;
    }

    public ArrayList<ReportEntry> getAllEntries()
    {
        ArrayList<ReportEntry> entries = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + KEY_FINISHED + " DESC";

        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null)
        {
            if(cursor.moveToFirst())
            {
                do {
                    ReportEntry entry = new ReportEntry();
                    entry.setIndex(cursor.getInt(0));
                    entry.setDate(cursor.getString(1));
                    entry.setStarted(cursor.getString(2));
                    entry.setFinished(cursor.getString(3));
                    entry.setCost(cursor.getInt(4));
                    entries.add(entry);
                } while(cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();

        return entries;
    }

    public void deleteEntry(int id)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + "=?", new String[]{Integer.toString(id)});
        db.close();
    }

    public void updateEntry(ReportEntry entry)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(KEY_DATE, entry.getDate());
        cv.put(KEY_STARTED, entry.getStarted());
        cv.put(KEY_FINISHED, entry.getFinished());
        cv.put(KEY_COST, entry.getCost());

        db.update(TABLE_NAME, cv, KEY_ID + "=?", new String[] { Integer.toString(entry.getIndex()) });

        db.close();
    }
}
