package com.labas.sample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.labas.seekarc.CircleButton;
import com.labas.seekarc.SeekArc;
import com.labas.seekarc.SeekArc.OnSeekArcChangeListener;

public class FuncActivity extends Activity {

    private CircleButton mButton;
    private SeekArc mSeekArc;
    private TextView mSeekArcProgress, mSeekArcProgressTimerText;

    private SharedPreferences sharedTime;
    private SharedPreferences.Editor sharedEditableContainer;

    private boolean updatedGUI = false;

    private void showToasty(String text) { Toast.makeText(FuncActivity.this, text, Toast.LENGTH_SHORT).show(); }
    protected int getLayoutFile() {
        return R.layout.activity_func;
    }

//    private void setMobileData(boolean enabled) {
//        ConnectivityManager dataManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
//        Method dataMtd = null;
//        try {
//            dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        }
//        assert dataMtd != null;
//        dataMtd.setAccessible(true);
//        try {
//            dataMtd.invoke(dataManager, enabled);
//        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutFile());

        mButton = (CircleButton) findViewById(R.id.mainButton);
        sharedTime = getSharedPreferences("FUNC_SHARED_TIME", MODE_WORLD_WRITEABLE);
        sharedEditableContainer = sharedTime.edit();

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        (findViewById(R.id.layout1)).getLayoutParams().height = size.y;

        mSeekArc = (SeekArc) findViewById(R.id.seekArc);
        mSeekArcProgress = (TextView) findViewById(R.id.seekArcProgress);
        mSeekArcProgressTimerText = (TextView) findViewById(R.id.seekArcProgressTimer);

        mSeekArc.setMax(60); // 60 Minutes
        mSeekArc.setProgress(1); // 1 Minute

        SeekBar mRotation = (SeekBar) findViewById(R.id.rotation);
        SeekBar mStartAngle = (SeekBar) findViewById(R.id.startAngle);
        SeekBar mSweepAngle = (SeekBar) findViewById(R.id.sweepAngle);
        SeekBar mArcWidth = (SeekBar) findViewById(R.id.arcWidth);
        SeekBar mProgressWidth = (SeekBar) findViewById(R.id.progressWidth);
//        CheckBox mRoundedEdges = (CheckBox) findViewById(R.id.roundedEdges);
        CheckBox mClockwise = (CheckBox) findViewById(R.id.clockwise);
        CheckBox mEnabled = (CheckBox) findViewById(R.id.enabled);

        mRotation.setProgress(mSeekArc.getArcRotation());
        mStartAngle.setProgress(mSeekArc.getStartAngle());
        mSweepAngle.setProgress(mSeekArc.getSweepAngle());
        mArcWidth.setProgress(mSeekArc.getArcWidth());
        mProgressWidth.setProgress(mSeekArc.getProgressWidth());

        //Apskritimo pakeitimai
        mSeekArc.setOnSeekArcChangeListener(new OnSeekArcChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onClick(SeekArc seekArc) {
                if (!mButton.isPressed()) {
                    mButton.setPressed(true);

                    Intent sBroadcast = new Intent(FuncActivity.this, BroadcastService.class);
                    sharedEditableContainer.putInt("TOTAL_BROWSE_TIME", seekArc.getProgress());
                    sBroadcast.putExtra("seekArc", seekArc.getProgress());
                    startService(sBroadcast);

                    (new Handler()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mButton.setPressed(false);
                        }
                    }, 2 * 1000);
                }
            }

            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                mSeekArcProgressTimerText.setText((!updatedGUI ? "min" : "sec"));
                mSeekArcProgress.setText(String.valueOf(progress));
            }
        });

        mRotation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar view, int progress, boolean fromUser) {
                mSeekArc.setArcRotation(progress);
                mSeekArc.invalidate();
            }
        });

        mStartAngle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar view, int progress, boolean fromUser) {
                mSeekArc.setStartAngle(progress);
                mSeekArc.invalidate();
            }
        });

        mSweepAngle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar view, int progress, boolean fromUser) {
                mSeekArc.setSweepAngle(progress);
                mSeekArc.invalidate();
            }
        });

        mArcWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar view, int progress, boolean fromUser) {
                mSeekArc.setArcWidth(progress);
                mSeekArc.invalidate();
            }
        });

        mProgressWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar view, int progress, boolean fromUser) {
                mSeekArc.setProgressWidth(progress);
                mSeekArc.invalidate();
            }
        });

//        mRoundedEdges.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                mSeekArc.setRoundedEdges(isChecked);
//                mSeekArc.invalidate();
//            }
//        });

        mClockwise.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSeekArc.setClockwise(isChecked);
                mSeekArc.invalidate();
            }
        });

        mEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSeekArc.setEnabled(isChecked);
                mSeekArc.invalidate();
            }
        });

    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

        if (sharedTime.getLong("STOP_TIME", 0) != 0) {
            getIntent().putExtra("likesLaikas", (long)(sharedTime.getInt("BROWSE_TIME", 0) * 1000) - (System.currentTimeMillis() - sharedTime.getLong("STOP_TIME", System.currentTimeMillis())));
            getIntent().putExtra("BROWSE_TIME", sharedTime.getInt("TOTAL_BROWSE_TIME", 0)); // Minutes
            updateGUI(getIntent());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
//        Log.i("BroadcastService-Func", "Registered broadcast receiver");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);

        if (!updatedGUI) {
            sharedEditableContainer.clear();
        } else {
            sharedEditableContainer.putLong("STOP_TIME", System.currentTimeMillis());
            sharedEditableContainer.putInt("BROWSE_TIME", mSeekArc.getProgress()); // Seconds
        }
        sharedEditableContainer.apply();
//        Log.i("BroadcastService-Func", "Unregistered broadcast receiver");
    }

    @Override
    public void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop();
    }

//    @Override
//    public void onDestroy() {
//        stopService(new Intent(this, BroadcastService.class));
//        Log.i("BroadcastService-Func", "Stopped service");
//        super.onDestroy();
//    }

    private void updateGUI(Intent intent) {
        if (!mSeekArc.isEnabled()) {
            showToasty("Funkcija išjungta!");
            return;
        } else if (mSeekArc.getProgress() == 0) {
            showToasty("Pasirinkai 0... Kodel?");
            return;
        }

        if (intent.getExtras() != null) {
            long likesLaikas = intent.getLongExtra("likesLaikas", -1);
            if (likesLaikas > 0) {
                if (!updatedGUI) {
                    updatedGUI = true;

                    mButton.setColor(Color.parseColor("#FF0000"));
                    mSeekArc.setMax((intent.getIntExtra("BROWSE_TIME", mSeekArc.getProgress()) * 60) - 1);
                }
                mSeekArc.setProgress((int) likesLaikas);
            } else {
                if (updatedGUI || mSeekArc.getProgress() == 0) {
                    updatedGUI = false;

                    mButton.setColor(Color.parseColor("#99CC00"));
                    mSeekArc.setMax(60);
                    mSeekArc.setProgress(1);
                }
            }
        }
    }
}
