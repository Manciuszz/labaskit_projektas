package com.labas.sample;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReportActivity extends Activity {

    ArrayList<ReportEntry> mContents;

    ReportAdapter mAdapter;

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        new WebTask().execute(new String[] {"http://www.laidoriteris.tk/reports.php"});

        mContents = new ArrayList<>();
        fetchContent();

//        String[] from = new String[] { "date", "started", "finished"};
//        int[] to = new int[] {R.id.listview_date, R.id.listview_started, R.id.listview_finished};

        mAdapter = new ReportAdapter(mContents, this);

        mListView = (ListView)findViewById(R.id.report_listview);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) findViewById(R.id.report_header);
                textView.setText(String.format("Naršymas - %d", position + 1));
            }
        });
    }

    private void fetchContent() {
//        SharedPreferences spref = getSharedPreferences("nData", Context.MODE_WORLD_WRITEABLE);
//        SharedPreferences.Editor spreEdit = spref.edit();

        ReportDBHandler handler = new ReportDBHandler(this);
//        handler.onUpgrade(handler.getWritableDatabase(), 0, 1); // Isvalyti duomenų bazę...
//        if (spref.getString("date","") != "")
//            handler.addEntry(new ReportEntry(0, spref.getString("date", "2016-12-01"), spref.getString("started", "00:00:00"), spref.getString("finished", "00:00:01"), spref.getInt("cost", 1)));
//        System.out.println("[DEBUG] Entry added");

        ArrayList<ReportEntry> entries = handler.getAllEntries();
        mContents = entries;

//        spreEdit.clear().apply();
    }

    class WebTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            StringBuilder builder = new StringBuilder();
            if(params.length > 0) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    InputStream in = con.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String line = null;
                    while((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            parseJson(s);
        }
    }

    public void parseJson(String json)
    {
        try {
            JSONObject object = new JSONObject(json);
            JSONArray array = object.getJSONArray("reports");
            if(mContents != null) {
                //mContents.clear();
                Toast.makeText(ReportActivity.this, "Duomenys užkrauti iš serverio!", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    ReportEntry entry = new ReportEntry(i, obj.getString("date"), obj.getString("started"), obj.getString("finished"), obj.getInt("cost"));
                    mContents.add(entry);
                }
                mAdapter.notifyDataSetChanged();
            }
        } catch(Exception e) {
            Toast.makeText(ReportActivity.this, "Nepavyko paimti duomenų iš serverio!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
