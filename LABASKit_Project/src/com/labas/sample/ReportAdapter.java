package com.labas.sample;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ReportAdapter extends BaseAdapter {

    ArrayList<ReportEntry> dataList;
    Activity activity;

    ReportAdapter(ArrayList<ReportEntry> d, Activity a) {
        dataList = d;
        activity = a;
    }

    public int getCount() {
        if (dataList != null) {
            return dataList.size();
        }
        return 0;
    }

    public long getItemId(int position) { return position; }

    public Object getItem(int position) {
        if(dataList != null) {
            return dataList.get(position);
        }
        return null;
    }

    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View vi = convertView;
        if(vi == null) {
            LayoutInflater li = LayoutInflater.from(activity);
            vi = li.inflate(R.layout.report_list_layout, null);
        }

        TextView dateText = (TextView)vi.findViewById(R.id.listview_date);
        TextView sText = (TextView)vi.findViewById(R.id.listview_started);
        TextView fText = (TextView)vi.findViewById(R.id.listview_finished);
        TextView cText = (TextView)vi.findViewById(R.id.listview_cost);
        ImageView bullet = (ImageView)vi.findViewById(R.id.listview_image);

        ReportEntry le = dataList.get(position);

        dateText.setText(le.getDate());
        sText.setText(String.format("Naršymo pradžia: %s", le.getStarted()));
        fText.setText(String.format("> %s", le.getFinished()));
        int cost = le.getCost();
        cText.setText(String.format("Sumokėta:\n%d € centa" + (cost % 10 == 1 && cost != 11 ? "s" : (cost % 10 > 9 ? "ų" : "i")), cost));
        if (cost > 5)
            bullet.setImageResource(R.drawable.custom_seek_arc_control_selector);
        else
            bullet.setImageResource(R.drawable.scrubber_control_focused_holo);

        return vi;
    }
}
