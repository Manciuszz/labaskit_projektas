package com.labas.sample;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

    private boolean paspaustasKarta = false;
    private GestureDetectorCompat gestureDetectorCompat;

    private Intent fIntent, rIntent, aIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSharedPreferences("FUNC_SHARED_TIME", MODE_WORLD_WRITEABLE).edit().clear().apply();

        final ImageView imageView = (ImageView) findViewById(R.id.swipeWiggle);

        imageView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideleftright));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sliderightleft));
            }
        }, (long) (1 * 1000 * 0.75));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidereset));
            }
        }, (long) (2 * 1000 * 0.75));

        fIntent = new Intent(MainActivity.this, FuncActivity.class);
        rIntent = new Intent(MainActivity.this, ReportActivity.class);
        aIntent = new Intent(MainActivity.this, AboutActivity.class);

        fIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        gestureDetectorCompat = new GestureDetectorCompat(this, new GestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                    }
                    result = true;
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }
                result = true;

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public void onSwipeLeft() {
        Toast.makeText(getBaseContext(), "Brūkšnys kairėn - Ataskaita", Toast.LENGTH_SHORT).show();
        startActivity(rIntent);
    }

    public void onSwipeRight() {
        Toast.makeText(getBaseContext(), "Brūkšnys dešinėn - Valdymo skydas", Toast.LENGTH_SHORT).show();
        startActivity(fIntent);
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
        Toast.makeText(getBaseContext(), "Brūkšnys apačion - Vartotojo vadovas", Toast.LENGTH_SHORT).show();
        startActivity(aIntent);
    }


    @Override
    public void onBackPressed() {
        int Seconds = 2;
        if (paspaustasKarta) {
            System.exit(1);
            return;
        }

        this.paspaustasKarta = true;
        Toast.makeText(this, "Paspausk dar kartą, kad išeitai!", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                paspaustasKarta = false;
            }
        }, Seconds * 1000);
    }
}