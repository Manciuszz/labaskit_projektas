## **Programos tikslas** ##
Prieš paaiškinant šios programos tikslą, pirmiausiai reiktų sužinoti, kad ši programa yra skirta "LABAS" mobiliojo ryšio operatoriaus vartotojams! Kodėl taip yra? Ogi tik "LABAS" turi unikalią interneto paslaugą pavadinimu "Minutė" t.y. "LABAS" vartotojai nemokamai gali užsisakyti paslaugą, kuri pradedant naršyti internete už 1 minutę naršymo leis sumokėti 0,01€ (1 EURO CENTĄ).

![labas_tut1.png](https://bitbucket.org/repo/yd5zjb/images/3106630688-labas_tut1.png)

Okey! Tai ka daro šita išmaniųjų įrenginių programėlė? Atsakymas trumpas ir aiškus - **taupome pinigus**.

...Kaip?

Pirmiausiai reikia sužinoti kaip ši paslauga veikia... Tarkime, kad mes pradedame naršyti internete 00:00:00 valandą. Galime pastebėti naudojimo išklotinėje (kurią galima rasti "Mano LABAS savitaronos svetainėje - http://mano.labas.lt), kad "LABAS" mobiliojo ryšio operatorius iš karto nuskaičiuos 0.01€ (1 EURO CENTĄ). Tai reiškia, kad pinigų nurašymas nuo sąskaitos vyksta minutės pradžioje, o ne pabaigoje.

Mes, vartotojai, naršydami internete tubūt niekada neatkreipiame dėmesio, kiek laiko mes tai darysime - tiesiog pasiekus mūsų tikslą, mes pabaigiame darbą ir išjungiame internetą. Tačiau, taip darydami mes neišvengsime papildomų išlaidų, kurios kauptųsi mums išjungiant internetą nelaiku. Pavyzdžiui - jeigu mes internete naršysime 1 minutę pradedant 00:00:00, kai ateis laikas 00:01:00 prasidės antroji minutė, už kurią turėsime sumokėti papildomą euro centą, o jeigu išjungsime interneto prieigą anksčiau, mes pilnai neišnaudosime mums priklausančio interneto laiko. Taigi, kaip visa to išvengti? Labai paprastai - pasinaudojant šia programėle!

Ši programėlė leidžia vartotojams pasirinkti kiek laiko internete jie norės naršyti internete ir už tai sumokėti nė vienu euro centu daugiau!

Tai įgyvendinama tiesiog sustabdant mobilio interneto prieigą išmaniajame įrenginyje likus vienai sekundei iki mūsų pasirinkto naršymo minutės laiko pabaigos.